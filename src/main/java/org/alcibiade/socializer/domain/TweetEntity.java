package org.alcibiade.socializer.domain;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class TweetEntity {

    private Long id;
    private String handle;
    private Date createdAt;
    private String body;
}
