package org.alcibiade.socializer.properties;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Map;

@Data
public class TwitterProperties {
    @NotBlank
    private String apiKey;

    @NotBlank
    private String apiSecretKey;

    @NotEmpty
    private Map<String, AccountProperties> accounts;

}
