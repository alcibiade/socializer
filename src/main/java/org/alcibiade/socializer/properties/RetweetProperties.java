package org.alcibiade.socializer.properties;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class RetweetProperties {
    @NotEmpty
    private List<String> users;

    private List<String> keywords;
}
