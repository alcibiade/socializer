package org.alcibiade.socializer.properties;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AccountProperties {
    @NotBlank
    private String accessToken;
    @NotBlank
    private String accessTokenSecret;

    private RetweetProperties retweet;

}
