package org.alcibiade.socializer.service;

import lombok.extern.slf4j.Slf4j;
import org.alcibiade.socializer.dao.TwitterDAO;
import org.alcibiade.socializer.domain.TweetEntity;
import org.alcibiade.socializer.properties.AccountProperties;
import org.alcibiade.socializer.properties.RetweetProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TwitterService {

    private final TwitterDAO twitterDAO;

    @Autowired
    public TwitterService(TwitterDAO twitterDAO) {
        this.twitterDAO = twitterDAO;
    }

    public void retweet(String accountName, AccountProperties accountProperties) {
        RetweetProperties retweetProperties = accountProperties.getRetweet();
        log.info("Re-tweeting for account {}", accountName);

        try {
            twitterDAO.open(accountProperties);

            Optional<Date> lastRT = twitterDAO.getLastRetweetTime();
            log.debug("  Last retweet by {} is at {}", accountName, lastRT);

            for (String handle : retweetProperties.getUsers()) {
                log.debug("  Considering re-tweeting {}", handle);

                Optional<TweetEntity> optionalTweet = twitterDAO.getMostRecentMessage(handle);

                if (!optionalTweet.isPresent()) {
                    log.debug("  No tweet found");
                    continue;
                }

                TweetEntity tweet = optionalTweet.get();

                List<String> keywords = retweetProperties.getKeywords();

                if (!acceptableDates(lastRT, tweet)) {
                    log.debug("    Tweet is older than my last message.");
                    continue;
                }

                if (!filterAccepts(tweet, keywords)) {
                    log.debug("    Tweet does not match keywords {}", keywords);
                    continue;
                }

                log.info("Re-tweeting tweet from {} created at {}", handle, tweet.getCreatedAt());
                twitterDAO.reTeweet(tweet.getId());
            }
        } finally {
            twitterDAO.close();
        }
    }

    private boolean filterAccepts(TweetEntity tweet, List<String> keywords) {
        if (keywords == null) {
            return true;
        }

        String body = tweet.getBody().toLowerCase();

        for (String term : keywords) {
            term = term.toLowerCase();

            if (body.contains(term)) {
                return true;
            }
        }

        return false;
    }

    private boolean acceptableDates(Optional<Date> lastRT, TweetEntity tweet) {
        if (lastRT.isPresent()) {
            return lastRT.get().before(tweet.getCreatedAt());
        } else {
            return true;
        }
    }
}
