package org.alcibiade.socializer.jobs;

import lombok.extern.slf4j.Slf4j;
import org.alcibiade.socializer.properties.SocializerProperties;
import org.alcibiade.socializer.service.TwitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RetweetJob {

    private TwitterService twitterService;

    private SocializerProperties socializerProperties;

    @Autowired
    public RetweetJob(TwitterService twitterService, SocializerProperties socializerProperties) {
        this.twitterService = twitterService;
        this.socializerProperties = socializerProperties;
    }

    @Scheduled(initialDelay = 5_000L, fixedRate = 3600_000L)
    public void retweet() {
        log.info("Processing retweets...");
        socializerProperties.getTwitter().getAccounts().entrySet().stream()
            .filter(entry -> entry.getValue().getRetweet() != null)
            .forEach(entry -> twitterService.retweet(entry.getKey(), entry.getValue()));

        log.info("Retweets complete");
    }
}
