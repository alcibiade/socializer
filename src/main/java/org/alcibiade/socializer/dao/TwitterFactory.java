package org.alcibiade.socializer.dao;

import org.alcibiade.socializer.properties.AccountProperties;
import org.alcibiade.socializer.properties.SocializerProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Component;

@Component
public class TwitterFactory {
    private final SocializerProperties socializerProperties;

    @Autowired
    public TwitterFactory(SocializerProperties socializerProperties) {
        this.socializerProperties = socializerProperties;
    }

    public Twitter createTwitterInstance(AccountProperties accountProperties) {
        Twitter twitter = new TwitterTemplate(
            socializerProperties.getTwitter().getApiKey(),
            socializerProperties.getTwitter().getApiSecretKey(),
            accountProperties.getAccessToken(),
            accountProperties.getAccessTokenSecret()
        );

        return twitter;
    }
}
