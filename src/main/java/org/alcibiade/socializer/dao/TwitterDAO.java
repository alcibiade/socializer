package org.alcibiade.socializer.dao;

import org.alcibiade.socializer.domain.TweetEntity;
import org.alcibiade.socializer.properties.AccountProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

@Component
public class TwitterDAO {

    private final TwitterFactory twitterFactory;
    private ThreadLocal<Twitter> twitterThreadLocal = new ThreadLocal<>();

    @Autowired
    public TwitterDAO(TwitterFactory twitterFactory) {
        this.twitterFactory = twitterFactory;
    }

    public void open(AccountProperties accountProperties) {
        Twitter twitter = twitterFactory.createTwitterInstance(accountProperties);
        twitterThreadLocal.set(twitter);
    }

    public void close() {
        // TODO: Cleanup twitter resources
    }

    public Optional<Date> getLastRetweetTime() {
        return twitterThreadLocal.get().timelineOperations().getUserTimeline().stream()
            .filter(Tweet::isRetweet)
            .map(Tweet::getCreatedAt)
            .findFirst();
    }

    public Optional<TweetEntity> getMostRecentMessage(String handle) {
        return twitterThreadLocal.get().timelineOperations().getUserTimeline(handle).stream()
            .filter(tweet -> !tweet.isRetweet())
            .map(tweet -> TweetEntity.builder()
                .id(tweet.getId())
                .handle(handle)
                .createdAt(tweet.getCreatedAt())
                .body(tweet.getText())
                .build()
            )
            .findFirst();
    }

    public void reTeweet(Long tweetId) {
        twitterThreadLocal.get().timelineOperations().retweet(tweetId);
    }
}
