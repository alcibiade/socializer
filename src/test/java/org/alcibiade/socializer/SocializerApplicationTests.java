package org.alcibiade.socializer;

import org.alcibiade.socializer.properties.AccountProperties;
import org.alcibiade.socializer.properties.SocializerProperties;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SocializerApplicationTests {

    @Autowired
    public SocializerProperties properties;

    @Test
    public void contextLoads() {
        Assertions.assertThat(properties).isNotNull();
        Assertions.assertThat(properties.getTwitter()).isNotNull();
        Assertions.assertThat(properties.getTwitter().getAccounts()).hasSize(3);

        AccountProperties account1 = properties.getTwitter().getAccounts().get("account1");

        Assertions.assertThat(account1.getAccessTokenSecret()).isEqualTo("dddddddddddddddddddddddddddddddddd");
        Assertions.assertThat(account1.getRetweet().getKeywords()).containsExactly("filterterm");
        Assertions.assertThat(account1.getRetweet().getUsers()).containsExactly("retweeteduser1", "retweeteduser2");

        AccountProperties account2 = properties.getTwitter().getAccounts().get("account2");

        Assertions.assertThat(account2.getAccessTokenSecret()).isEqualTo("ffffffffffffffffffffffffffffffffff");
        Assertions.assertThat(account2.getRetweet()).isNull();

        AccountProperties account3 = properties.getTwitter().getAccounts().get("account3");

        Assertions.assertThat(account3.getAccessTokenSecret()).isEqualTo("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
        Assertions.assertThat(account3.getRetweet().getKeywords()).isNull();
        Assertions.assertThat(account3.getRetweet().getUsers()).isNotEmpty();
    }

}

