FROM openjdk:11-jdk as BUILD

WORKDIR /usr/src/app
COPY . .

RUN ./gradlew --no-daemon build

FROM openjdk:11-jre

COPY --from=BUILD /usr/src/app/build/libs /opt/target
COPY application-production.yml /opt/target
WORKDIR /opt/target

CMD ["/bin/bash", "-c", "find -type f -name 'socializer-*.jar' | xargs java -Dspring.profiles.active=production -Xmx64m -jar"]
